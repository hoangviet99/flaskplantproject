from flask import Flask, request, jsonify

from app.torch_utils import transform_image, get_prediction

app = Flask(__name__)

LABEL_ARR = ['bo-cong-anh', 'cam-tu-cau', 'hoa-giay', 'hoa-hong', 'xac-phao-do']

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/predict', methods=['POST'])
def predict():
    if request.method =='POST':
        file = request.files.get('file')
        if file is None or file.filename == "":
            return jsonify({'error':'no file'})
        if not allowed_file(file.filename):
            return jsonify({'error':'format not supported'})

        try:
            img_bytes = file.read()
            tensor = transform_image(img_bytes)
            prediction = get_prediction(tensor)
            print(prediction[0])
            return jsonify({
            'status': 'success',
            'accuracy': str(prediction[0]),
            'predict': str(prediction[1]),
            'label': LABEL_ARR[prediction[1]],
        })     
        except:
            return jsonify({
            'error': 'Prediction error!'
        })

# if __name__ == '__main__':
#     app.run(debug=True)

# export FLASK_APP=main.py
# export FLASK_ENV=development
# flask run